import './App.css';
import { useState, useEffect } from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import UserContext from './context/UserContext';
import Axios from 'axios';


// composants
import Navigation from './components/Nav';
import Home from './components/page/Home';
  import All from './components/All';
import FoodPorn from './components/page/FoodPorn'  
import Account from './components/page/Account';
import Register from './components/auth/Register';
import Login from './components/auth/Login';

import Recipies from './components/backoffice/Recipies';
import CreateRecipe from './components/backoffice/CreateRecipe';
import Photos from './components/backoffice/Photos'





function App() {

  const [userData, setUserData] = useState({
    token: undefined,
    user: undefined,
  });

  useEffect( () => {
     const checkLoggedIn = async () => {
        const token = localStorage.getItem("auth-token");
        if(token === null){
          localStorage.setItem("auth-token", "");
          token= "";
        }
        const tokenRes = await Axios.post(
          "http://localhost:8080/users/tokenIsValid", 
          null, 
          { headers: { "x-auth-token": token } }
        );
        // console.log(tokenRes.data)
        if(tokenRes.data){
          const userRes = await Axios.get("http://localhost:8080/users/", 
          {headers: { "x-auth-token": token }}
          );
          setUserData({
            token,
            user: userRes.data
          });
        }
     };
     checkLoggedIn();
  }, [] );

  return (
    <div className="App">
        <Router>
          <UserContext.Provider value={{userData, setUserData}}>
            <Navigation />

            <Switch>

              <Route path='/' exact component={Home} ></Route>
              <Route path='/all-recipes' exact component={All} ></Route>
              <Route path='/food-porn' component={FoodPorn}></Route>


              {/* auth */}
              <Route path='/register' exact component={Register} ></Route>
              <Route path='/login' exact component={Login} ></Route>


              {/* backoffice */}
              <Route path='/account' exact component={Account}></Route>
              <Route path='/account/my-recipes' component={Recipies}></Route>
              <Route path='/account/create-recipe' component={CreateRecipe}></Route>
              <Route path='/account/photos' component={Photos}></Route>

            </Switch>

          </UserContext.Provider>
        </Router>
      


    </div>
  );
}

export default App;
