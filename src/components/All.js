import React , {useState, useEffect} from 'react'
import { Card, Container, Row, Col, Button } from 'react-bootstrap';
import RecipeAPI from '../service/RecipeAPI';


const AllRecipes = () => {

    const [recipes, setRecipes] = useState([]);
    const [ search, setSearch] = useState(); 

    // searchbar
    const filteredRecipe = recipes.filter(r => 
            r.title.toLower
        );



    const fetchRecipes = async (id) => {
        try{
            const data = await RecipeAPI.findAll(); 
            setRecipes(data);           
        } catch (error) {
            console.log(error.response)
        } 
    }
      
    useEffect( () => {
        fetchRecipes();            
    }, []);



    return (

        <>
        <div className="allRecipe-container">
            <Container>
                <Row>
                {recipes.map( recipe => (

                    <Col lg={6} className="card-container" key={recipe.id}>
                        <div className="card shadow">
                                <Row>
                                    <Col lg={4}>
                                        <div className="card-recette-img-container">
                                            <img src="https://picsum.photos/200/250" alt="" className="card-recette-img"/>
                                        </div>                                        
                                    </Col>
                                    <Col lg={8}>
                                        <div className="card-recette-body">
                                            <h3>{recipe.title}</h3>
                                            <h4>{recipe.subtitle}</h4>
                                            <div className="button-group-recette">
                                                <Button className="btn-all btn-all-prev" >Prévisualiser</Button> 
                                                <Button className="btn-all btn-all-voir" >Voir plus</Button> 
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                        </div>
                    </Col>                   
                ))}
                </Row>
            </Container>
        </div>
        </>
    );

}
export default AllRecipes;