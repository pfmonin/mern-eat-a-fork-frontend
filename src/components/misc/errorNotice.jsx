import React from 'react'
import {Alert} from 'react-bootstrap'

export default function NoticeError (props) {

    return (
        <>
        <Alert variant="danger">
        <span> {props.message}         
            {/* <button onClick={props.clearError}></button>    */}
        </span>         
        </Alert>
        
        </>
    );
}; 