import React, {useState, useContext} from 'react'; 
import { useHistory } from 'react-router-dom'
import UserContext from '../../context/UserContext'
import { Form, Button } from 'react-bootstrap'
import Axios from 'axios'
import '../../assets/css/style.css'
import logo from '../../assets/image/node.png'
import ErrorNotice from '../misc/errorNotice'



const LoginAction = () => {

    const [username, setUsername] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [error, setError] = useState();
    

    const {setUserData} = useContext(UserContext);
    const history = useHistory();

    const submit = async (e) => {
        e.preventDefault();

        try {
            const loginRes = await Axios.post("http://localhost:8080/users/login",{
            username,
            email, 
            password,
        });
        setUserData({
            token: loginRes.data.token,
            user : loginRes.data.user
        });
        localStorage.setItem("auth-token", loginRes.data.token); 
        history.push("/");
            
        } catch (error) {
            error.response.data.message && setError(error.response.data.message);   
        }
    };


    return(

        <>

        <div className="login-page">
            <div className="form-login-container">
                <div className=" shadow container login-form ">
                    <img src={logo} className="logo" alt="logo-node" />
                    <h2 className="authTitle" > - login -</h2>
                    {error && (
                        <ErrorNotice message={error} clearError={() => setError(undefined)} />
                    )};
                    <Form onSubmit={submit}>

                    <Form.Group controlId="login-username">                        
                            <Form.Control type="text" placeholder="Enter username" onChange={(e) => setUsername(e.target.value)} />               
                        </Form.Group>

                        <Form.Group controlId="login-email">                       
                            <Form.Control type="email" placeholder="Enter email"  onChange={(e) => setEmail(e.target.value)}/>             
                        </Form.Group>

                        <Form.Group controlId="login-password">                        
                            <Form.Control type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />                 
                        </Form.Group>
                                    

                        <Button className="authButton" type="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            </div>
        </div>


        </>

    );
};

export default LoginAction; 