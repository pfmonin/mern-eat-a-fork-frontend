import React, {useState, useContext} from 'react'; 
import { useHistory } from 'react-router-dom'
import UserContext from '../../context/UserContext'
import { Form, Button } from 'react-bootstrap'
import Axios from 'axios'
import '../../assets/css/style.css'
import logo from '../../assets/image/node.png'
import ErrorNotice from '../misc/errorNotice'


export default function Register () {

    const [username, setUsername] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [passwordCheck, setPasswordCheck] = useState();
    const [error, setError] = useState();

    const {setUserData} = useContext(UserContext);
    const history = useHistory();

    const submit = async (e) => {
        e.preventDefault();

        try {

            const newUser = { username, email, password, passwordCheck};
            const registerRes = await Axios.post("http://localhost:8080/users/register", newUser);        
    
            const loginRes = await Axios.post("http://localhost:8080/users/login",{
                username,
                email, 
                password,
            });
            setUserData({
                token: loginRes.data.token,
                user : loginRes.data.user
            });
            localStorage.setItem("auth-token", loginRes.data.token); 
            history.push("/");
            
        } catch (error) {
            error.response.data.message && setError(error.response.data.message);            
        }
    };



    return (
        <>
        <div className="register-page">

            <div className="form-register-container">

                <div className=" shadow containr register-form ">
                        <img src={logo} className="logo" alt="logo-node" />
                        <h2 className="authTitle" > - Register -</h2>                    
                        {error && (
                        <ErrorNotice message={error} clearError={() => setError(undefined)} />
                    )};
                    <Form onSubmit={submit}>

                        <Form.Group controlId="register-username">                        
                            <Form.Control type="text" placeholder="Enter username" onChange={(e) => setUsername(e.target.value)} />                       
                        </Form.Group>

                        <Form.Group controlId="register-email">                       
                            <Form.Control type="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />                        
                        </Form.Group>

                        <Form.Group controlId="register-password">                        
                            <Form.Control type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
                        </Form.Group>

                        <Form.Group controlId="register-assword-check">                        
                            <Form.Control type="password"  placeholder="Repeat password" onChange={(e) => setPasswordCheck(e.target.value)} />
                        </Form.Group>                   

                        <Button className="authButton" type="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            </div>
        </div>

        </>
    );
};

