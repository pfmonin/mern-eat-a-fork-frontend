import React, {useState, useEffect, useContext} from 'react'
import UserContext from '../../context/UserContext'
import Axios from 'axios'
import {Form, Button } from 'react-bootstrap'
import logo from '../../assets/image/node.png'
import ErrorNotice from '../misc/errorNotice'
import BackNav from '../backoffice/Back-nav'
import UserApi from '../../service/UserAPI'
import jwt_decode from "jwt-decode";
 

const CreateRecipe = () => {

    const token= localStorage.getItem('auth-token');        
    const decoded = jwt_decode(token); 
    const  id = decoded.id    

    const [title, setTitle] = useState();
    const [subtitle, setSubtitle] = useState();
    const [description, setDescription] = useState();
    const [cookingTime, setCookingTime] = useState();
    const {setUserData} = useContext(UserContext);    
    

    console.log(setUserData);

    const handleSubmit = async(e) => {
        e.preventDefault(); 

        try {          
            
            const recipe = { title, subtitle, description, cookingTime, userId:id } 
            const newRecipe = await Axios
                    .post(`http://localhost:8080/recipes/create`,
                    recipe,
                    {headers : {
                        'x-auth-token' : token,
                        'Accept' : 'application/json',
                        'Content-Type': 'application/json'
                        }
                    })
                    .then(response => response.data)                   
                    console.log(newRecipe);                    

        } catch (error) {
            console.log(error)
        }
    } 


    return (

        <>
        <div className="create-recipe-page">

            <BackNav />

            <div className=" container form-create-recipe-container">
                <div className=" shadow containr create-form ">
                        <img src={logo} className="logo" alt="logo-node" />
                        <h2 className="authTitle" > - Create Recipe -</h2>                        
                            <Form onSubmit={handleSubmit}>

                                <Form.Group controlId="recipe-title">                        
                                    <Form.Control type="text" placeholder="Enter title" onChange={(e) => setTitle(e.target.value)} />                       
                                </Form.Group>

                                <Form.Group controlId="recipe-subtitle">                       
                                    <Form.Control type="text" placeholder="Enter subtitle" onChange={(e) => setSubtitle(e.target.value)} />                        
                                </Form.Group>

                                <Form.Group controlId="recipe-description">                        
                                    <Form.Control type="text" placeholder="Description" onChange={(e) => setDescription(e.target.value)} />
                                </Form.Group>

                                <Form.Group controlId="recipe-time">                        
                                    <Form.Control type="text"  placeholder="Enter Cooking time" onChange={(e) => setCookingTime(e.target.value)} />
                                </Form.Group>  

                                <Button className="authButton" type="submit">
                                    Submit
                                </Button>
                            </Form>
                        </div>
                    </div>
                </div>


        </>

    ); 
}
export default CreateRecipe;
