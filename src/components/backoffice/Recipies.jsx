import React, {useState, useEffect} from 'react'; 
import BackNav from './Back-nav'
import RecipeAPI from '../../service/RecipeAPI'


const Recipes = props => {

    const [recipes, setRecipes] = useState([]); 

    // barre de recherche 

    // pagination

    // find all
    const fetchRecipes = async (id) => {        

        try{
            const data = await RecipeAPI.findAllFromId(); 
            setRecipes(data); 
            console.log(data); 
           
        } catch (error) {
            console.log(error.response)
        } 
    }
      
    useEffect( () => {
        fetchRecipes();            
    }, []);

   


    return(
        <>
        <BackNav />


            
        </>
    );
}
export default Recipes;