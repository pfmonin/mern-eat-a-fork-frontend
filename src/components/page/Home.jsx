import React from 'react'

const HomePage = () => {

    // const gsap = "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js"
    // console.log (gsap);

    // const tl = gsap.timeline({ defaults : {ease : "power1.out"}})


    return(

        <>
        <div className="home">        
            <div className="home-main-title">
                <h1> Eat a fork ! </h1>
                <h2>Your new best kitchen companion</h2>
            </div>
        </div>

        <div className="intro">
            <h1 className="hide">
                <span className="text">Be Creative<span className="dot">.</span></span>
            </h1>
            <h1 className="hide">
                <span className="text">Make it tasty<span className="dot">.</span></span>
            </h1>
            <h1 className="hide">
                <span className="text">Turn it into experiment<span className="dot">.</span></span>
            </h1>
        </div>

        <div className="slider"></div>

        </>

    );
};

export default HomePage; 