import Axios from 'axios'

function  findLoggedUser () {

    const token = localStorage.getItem('auth-token');

    return Axios
     .get('http://localhost:8080/users/',{'x-auth' : token})
    .then(response => console.log(response.json.username));
}
export default findLoggedUser;