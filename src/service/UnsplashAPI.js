import React from 'react'
import Axios from 'axios'

function FindFoodImg () {

    const client_id = process.env.REACT_APP_US_ACCESS;
    const apiRoot = "https://api.unsplash.com";

    return Axios
    .get(`${apiRoot}/photos/random?client_id=${client_id}&count=20&query=food&orientation=landscape`)
    .then(response => response.data);
}
export default {
    FindFoodImg
};